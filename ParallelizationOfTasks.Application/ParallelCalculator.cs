﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ParallelizationOfTasks.Application
{
    public class ParallelCalculator : ICalculator
    {
        private const int TreadCount = 10;
        public (long, long) Sum(IEnumerable<int> arr)
        {
            object locker = new object();

            var resaltList = new List<long>();

            var sw = new Stopwatch();

            sw.Start();

            var sizePart = arr.Count() / TreadCount;

            for (int i = 0; i < TreadCount; i++)
            {
                var arrQuery = arr.Skip(sizePart * i);

                var arrPart = i < TreadCount - 1
                    ? arrQuery.Take(sizePart).ToList()
                    : arrQuery.ToList();

                var thread = new Thread(() =>
                {
                    long resalt = 0;

                    foreach (var item in arrPart)
                    {
                        resalt += item;
                    }

                    lock (locker)
                    {
                        resaltList.Add(resalt);
                    }
                });

                thread.Start();
                thread.Join();
            }

            sw.Stop();

            return (sw.ElapsedMilliseconds, resaltList.Sum());
        }
    }
}
