﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelizationOfTasks.Application
{
    public interface ICalculator
    {
        (long, long) Sum(IEnumerable<int> arr);
    }
}
