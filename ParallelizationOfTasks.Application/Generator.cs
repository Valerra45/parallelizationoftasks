﻿using System;
using System.Collections.Generic;

namespace ParallelizationOfTasks.Application
{
    public class Generator
    {
        public static IEnumerable<int> Generete(int size)
        {
            int[] arr = new int[size];

            var rnd = new Random();

            for (var i = 0; i < size; i++)
            {

                arr[i] = rnd.Next(10);
            }

            return arr;
        }
    }
}
