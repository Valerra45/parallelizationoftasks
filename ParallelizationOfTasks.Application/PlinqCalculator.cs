﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelizationOfTasks.Application
{
    public class PlinqCalculator : ICalculator
    {
        public (long, long) Sum(IEnumerable<int> arr)
        {
            long resalt = 0;

            var sw = new Stopwatch();

            sw.Start();

            resalt = arr.AsParallel().Sum();

            sw.Stop();

            return (sw.ElapsedMilliseconds, resalt);
        }
    }
}
