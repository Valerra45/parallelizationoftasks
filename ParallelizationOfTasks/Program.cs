﻿using ParallelizationOfTasks.Application;
using System;
using System.Collections.Generic;

namespace ParallelizationOfTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> sizes = new List<int>() { 100000, 1000000, 10000000 };

            Dictionary<string, ICalculator> calculators = new Dictionary<string, ICalculator>();
            calculators.Add("Custom", new СustomCalculator());
            calculators.Add("Parallel", new ParallelCalculator());
            calculators.Add("PLINQ", new PlinqCalculator());

            foreach (var size in sizes)
            {
                var arr = Generator.Generete(size);

                foreach (var calculator in calculators)
                {
                    var resalt = calculator.Value.Sum(arr);

                    Console.WriteLine($"Arrey size: {size}. {calculator.Key} sum: {resalt.Item2} time: {resalt.Item1} mc");
                }

                Console.WriteLine();
            }

        }
    }
}
